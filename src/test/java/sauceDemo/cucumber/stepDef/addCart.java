package sauceDemo.cucumber.stepDef;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.messages.types.Product;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class addCart {
    public WebDriver driver;
    String baseUrl = "https://www.saucedemo.com/";
    private String products;

    @Given("user is loged in")
    public void userIsLogedIn() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get(baseUrl);
        driver.manage().window().maximize();
        driver.findElement(By.cssSelector("#user-name")).sendKeys("standard_user");
        driver.findElement(By.cssSelector("#password")).sendKeys("secret_sauce");
        driver.findElement(By.cssSelector("#login-button")).click();

        String title = driver.findElement(By.xpath("//*[@class='title']")).getText();
        Assert.assertEquals("Products", title);
    }

    @When("user click the add to cart button")
    public void userClickTheAddToCartButton(){
        products = driver.findElement(By.xpath("//*[@id='item_4_title_link']/div")).getText();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElement(By.id("add-to-cart-sauce-labs-backpack")).click();
        String remove = driver.findElement(By.id("remove-sauce-labs-backpack")).getText();
        Assert.assertEquals("Remove", remove);
    }

    @And("user click the cart button")
    public void userClickTheCartButton() {
        driver.findElement(By.id("shopping_cart_container")).click();
        String title = driver.findElement(By.xpath("//*[@class='title']")).getText();
        Assert.assertEquals("Your Cart", title);
    }

    @Then("user view the selected products")
    public void userViewTheSelectedProducts() {
        Assert.assertEquals("Sauce Labs Backpack", products);
    }
    @When("user click sidebar")
    public void userClickSidebar() {
        driver.findElement(By.xpath("//button[contains(text(), 'Open Menu')]")).click();
    }

    @And("user click the logout button")
    public void userClickTheLogoutButton() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.cssSelector("#logout_sidebar_link")).click();
    }

    @Then("user view login page")
    public void userViewLoginPage() {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        String title = driver.findElement(By.className("login_logo")).getText();
        Assert.assertEquals("Swag Labs", title);
    }

    @And("user click the checkout button")
    public void userClickTheCheckoutButton() {
        driver.findElement(By.id("checkout")).click();
        String title = driver.findElement(By.xpath("//*[@id='header_container']/div[2]/span")).getText();
        Assert.assertEquals("Checkout: Your Information", title);
    }

    @And("user fill the form")
    public void userFillTheForm() {
        driver.findElement(By.id("first-name")).sendKeys("Juni");
        driver.findElement(By.id("last-name")).sendKeys("Lee");
        driver.findElement(By.id("postal-code")).sendKeys("66552");
    }

    @And("user click the continue button")
    public void userClickTheContinueButton() {
        driver.findElement(By.id("continue")).click();
    }

    @Then("user view overview checkout")
    public void userViewOverviewCheckout() {
        String title = driver.findElement(By.xpath("//*[@id='header_container']/div[2]/span")).getText();
        Assert.assertEquals("Checkout: Overview", title);
    }

    @And("user click the finish button")
    public void userClickTheFinishButton() {
        driver.findElement(By.id("finish")).click();
    }

    @Then("user view success page")
    public void userViewSuccessPage() {
        String success = driver.findElement(By.xpath("//*[@id='checkout_complete_container']/h2")).getText();
        Assert.assertEquals("Thank you for your order!", success);
        driver.findElement(By.id("back-to-products")).click();
        driver.close();
    }
}
