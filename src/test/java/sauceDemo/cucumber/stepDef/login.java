package sauceDemo.cucumber.stepDef;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class login {
    WebDriver driver;
    String baseUrl = "https://www.saucedemo.com/";

    @Given("halaman login")
    public void halamanLogin() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get(baseUrl);
        driver.manage().window().maximize();

        String title = driver.findElement(By.xpath("//*[@id='root']/div/div[1]")).getText();
        Assert.assertEquals("Swag Labs", title);
    }

    @When("input unregistered username")
    public void inputUnregisteredUsername() {
        driver.findElement(By.cssSelector("#user-name")).sendKeys("Riska");
    }

    @And("input unregistered password")
    public void inputUnregisteredPassword() {
        driver.findElement(By.cssSelector("#password")).sendKeys("123");
    }

    @And("click the login button")
    public void clickTheLoginButton() {
        driver.findElement(By.cssSelector("#login-button")).click();
    }

    @Then("user get error message")
    public void userGetErrorMessage() {
        String warn = driver.findElement(By.xpath("//h3[@data-test='error']")).getText();
        Assert.assertEquals("Epic sadface: Username and password do not match any user in this service", warn);
        driver.close();
    }

    @When("input registered username")
    public void inputRegisteredUsername() {
        driver.findElement(By.cssSelector("#user-name")).sendKeys("standard_user");
    }

    @And("input registered password")
    public void inputRegisteredPassword() {
        driver.findElement(By.cssSelector("#password")).sendKeys("secret_sauce");
    }

    @Then("user in dashboard page")
    public void userInDashboardPage() {
        String title = driver.findElement(By.xpath("//*[@class='title']")).getText();
        Assert.assertEquals("Products", title);
        driver.close();
    }

}
