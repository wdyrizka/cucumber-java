Feature: Add products to the cart
  @Regression @Checkout
  Scenario: user can add products
    Given user is loged in
    When user click the add to cart button
    And user click the cart button
    Then user view the selected products
    And user click the checkout button
    And user fill the form
    And user click the continue button
    Then user view overview checkout
    And user click the finish button
    Then user view success page