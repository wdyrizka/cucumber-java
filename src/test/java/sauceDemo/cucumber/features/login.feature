Feature: login
  @Regression
  Scenario: Login with unregistered account
    Given halaman login
    When input unregistered username
    And input unregistered password
    And click the login button
    Then user get error message

  @Regression
  Scenario: Login with registered account
    Given halaman login
    When input registered username
    And input registered password
    And click the login button
    Then user in dashboard page
